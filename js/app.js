const imagen = document.querySelector('.card-body-img');
const nombre = document.querySelector('.card-body-title')
const tipo = document.querySelector('.card-body-type')
const exp = document.querySelector('.card-body-text')
const ataque = document.querySelectorAll('.card-footer-social h3')
const defensa = document.querySelectorAll('.card-footer-social h3')
const  especial = document.querySelectorAll('.card-footer-social h3')


const pintNotFound = () =>{
    nombre.innerHTML = 'No encontrado'
    imagen.setAttribute('src', 'images/poke-shadow.png')
    tipo.innerHTML =  ''
    exp.innerHTML = ''
    ataque[0].innerHTML = ''
    defensa[1].innerHTML = ''
    defensa[2].innerHTML = ''
    imagen.style.background = 'white'
}

document.addEventListener('DOMContentLoaded',()=>{
    const random = getRandomInit(1, 151)
    fetchData(random)
});

const getRandomInit = (min,max) =>  {
    return Math.floor(Math.random() * (max - min)) + min
}

const obtenerPokemon = document.getElementById('buscarPokemon');
const nombrePokemon = document.getElementById('nombrePoke');


obtenerPokemon.addEventListener('click', ()=>{  
    fetchData(nombrePokemon.value.toLowerCase())
})
nombrePokemon.addEventListener('keypress', (e)=>{
    let code = (e.key);
    if(code=='Enter'){
        fetchData(nombrePokemon.value.toLowerCase())
    }
}) 

const fetchData = async(nombre) =>{
    try{
        const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${nombre}`)
        const data = await res.json()

        const {types} = data

        const pokemon = {
            img: data.sprites.front_default,
            nombre: data.name,
            hp: data.stats[0].base_stat,
            experiencia: data.base_experience,
            ataque: data.stats[1].base_stat,
            defensa: data.stats[2].base_stat,
            especial: data.stats[3].base_stat

        }
        pintCard(pokemon,types)
        setCardColor(types)
        
    }catch(error){
        pintNotFound()
    }

}
const typeColors = {
    electric: '#FFEA70',
    fairy:  '#feabff',
    normal: '#B09398',
    fire: '#FF675C',
    water: '#0596C7',
    ice: '#AFEAFD',
    rock: '#999799',
    flying: '#7AE7C7',
    grass: '#4A9681',
    psychic: '#FFC6D9',
    ghost: '#561D25',
    bug: '#A2FAA3',
    poison: '#795663',
    ground: '#D2B074',
    dragon: '#DA627D',
    steel: '#1D8A99',
    fighting: '#2F2F2F',
    default: '#2A1A1F',
    
};

const setCardColor = types =>{
    const colorOne = typeColors[types[0].type.name]
    const colorTwo = types[1] ? typeColors[types[1].type.name] : typeColors.default;
    imagen.style.background = `radial-gradient(${colorTwo} 30%, ${colorOne} 30%)`
    imagen.style.backgroundSize = '5px 5px'
}


const pintCard = (pokemon,types) =>{

    tipo.innerHTML = '';

    types.forEach(type => {
        const tipoPokemon = document.createElement('h4') 
        tipoPokemon.classList.add('tipos')
        tipoPokemon.style.color = typeColors[type.type.name]
        tipoPokemon.textContent = type.type.name
        tipo.appendChild(tipoPokemon)
    });  

    nombre.innerHTML = ` ${pokemon.nombre} <span>${pokemon.hp} hp</span>`
    imagen.setAttribute('src',pokemon.img) 
    exp.textContent = pokemon.experiencia + ' exp'
    ataque[0].textContent = pokemon.ataque + 'K'
    defensa[1].textContent = pokemon.defensa + 'K'
    defensa[2].textContent = pokemon.especial + 'K'

} 
